<?php

namespace Drupal\ppss;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Service Api.
 */
class PPSSServiceApi {

  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Creates PPSS Service API object.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current account service.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   The logger service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   */
  public function __construct(
    AccountInterface $account,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entityTypeManager,
    LoggerChannelFactory $loggerFactory,
    MailManagerInterface $mail_manager
  ) {
    $this->account = $account;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entityTypeManager;
    $this->loggerFactory = $loggerFactory;
    $this->mailManager = $mail_manager;
  }

  /**
   * Send emails to the customer about their subscription.
   *
   * @param string $user_email
   *   User email.
   * @param string $type
   *   Type mail.
   * @param int|null $finish_date
   *   Subscription end date.
   * 
   * @return bool
   *   Return true if the email was sent.
   */
  public function sendEmail($user_email, $type, $finish_date) {
    $config = $this->configFactory->get('ppss.settings');
    if ($config->get($type)) {
      $module = 'ppss';
      $key = 'ppss_email';
      $to = $user_email . ';' . $this->configFactory->get('system.site')->get('mail');
      $langcode = $this->account->getPreferredLangcode();
      $send = TRUE;
      $params = [];
      switch ($type) {
        case 'mail_canceled_subject':
          $params['subject'] = $config->get('mail_canceled_subject');
          $params['message'] = $config->get('mail_canceled_body');
          break;

        case 'mail_scheduled_cancellation_subject':
          $params['subject'] = $config->get('mail_scheduled_cancellation_subject');
          $body = $config->get('mail_scheduled_cancellation_body');
          $params['message'] = str_replace('[finish-date]', date('d/m/Y', $finish_date), $body);
          break;

        default:
          $this->loggerFactory->get('PPSS')->info('Received unknown mail');
      }
      $result = $this->mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
      if ($result['result']) {
        $this->loggerFactory->get('PPSS')->info('An email notification has 
          been sent to @email ', ['@email' => $to]);
        return TRUE;
      }
      else {
        $this->loggerFactory->get('PPSS')->error('There was a problem sending 
          your message and it was not sent @email.', ['@email' => $to]);
        return FALSE;
      }
    }
    return FALSE;
  }

  /**
   * Unpublish all user ads.
   *
   * @param int $uid
   *   User id.
   * @param string $expire
   *   Date to unpublish content.
   * @param boolean $set_unpublished
   *   TRUE if Unpublish the node immediately.
   * 
   * @return bool
   *   Return true if the node was saved.
   */
  public function unpublishNodes($uid, $expire, $set_unpublished = FALSE) {
    // Get all user ads.
    $nids = $this->entityTypeManager->getStorage('node')
      ->getQuery()->condition('uid', $uid)
      ->accessCheck(FALSE)
      ->condition('status', 1)
      ->execute();
    $entity = $this->entityTypeManager->getStorage('node');
    $nodes = $entity->loadMultiple($nids);

    foreach ($nodes as $node) {
      if ($set_unpublished) {
        // Unpublish node
        $node->setUnpublished();
      } else {
        // Unpublish node setting a date in the future
        $node->set('unpublish_on', $expire);
      }
      $node->save();
    }
    return TRUE;
  }

}

<?php

namespace Drupal\ppss\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the PPSS Configuration Settings form.
 */
class PPSSFormSettings extends ConfigFormBase {
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * Constructs an PPSSFormSettings object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    PathValidatorInterface $path_validator
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->pathValidator = $path_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('path.validator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    // Unique ID of the form.
    return 'ppss_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'ppss.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get the internal node type machine name.
    $existingContentTypeOptions = $this->getExistingContentTypes();
    $paymentGateways = ['Stripe' => 'Stripe'];

    // The settings needed was configured inside ppss.settings.yml file.
    $config = $this->config('ppss.settings');

    // General settings.
    $form['allowed_gateway'] = [
      '#type' => 'radios',
      '#title' => $this->t('Subscriptions can be pay with the selected payment gateway.'),
      '#options' => $paymentGateways,
      '#default_value' => $config->get('allowed_gateway'),
      '#description' => $this->t('Select the payment gateway you like to use to enable it.'),
      '#required' => TRUE,
    ];

    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('The content types to enable PPSS button for'),
      '#default_value' => $config->get('content_types'),
      '#options' => $existingContentTypeOptions,
      '#empty_option' => $this->t('- Select an existing content type -'),
      '#description' => $this->t('On the specified node types, an PPSS button
        will be available and can be shown to make purchases.'),
      '#required' => TRUE,
    ];

    $form['return_url'] = [
      '#type' => 'details',
      '#title' => $this->t('URL of return pages'),
      '#open' => TRUE,
    ];

    $form['return_url']['success_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Success URL'),
      '#default_value' => $config->get('success_url'),
      '#description' => $this->t('What is the return URL when a new successful sale was made? Specify a relative URL.'),
      '#size' => 40,
      '#required' => TRUE,
    ];

    $form['return_url']['error_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Error URL'),
      '#default_value' => $config->get('error_url'),
      '#description' => $this->t('What is the return URL when a sale fails? Specify a relative URL.
        Leave blank to display the default front page.'),
      '#size' => 40,
      '#required' => TRUE,
    ];

    $form['email'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Emails'),
    ];
    // Email sent on the day of cancellation.
    $form['subscription_canceled'] = [
      '#type' => 'details',
      '#title' => $this->t('Subscription canceled'),
      '#open' => TRUE,
      '#description' => $this->t('Edit cancellation emails sent when users unsubscribe.'),
      '#group' => 'email',
    ];
    $form['subscription_canceled']['mail_canceled_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $config->get('mail_canceled_subject'),
      '#maxlength' => 180,
    ];
    $form['subscription_canceled']['mail_canceled_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $config->get('mail_canceled_body'),
      '#rows' => 10,
    ];
    // Email sent when the user cancels their subscription before
    // the next payment.
    $form['schedule_cancellation'] = [
      '#type' => 'details',
      '#title' => $this->t('Schedule unsubscription'),
      '#open' => FALSE,
      '#description' => $this->t('Edit cancellation email. Available variables are: [finish-date]'),
      '#group' => 'email',
    ];
    $form['schedule_cancellation']['mail_scheduled_cancellation_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $config->get('mail_scheduled_cancellation_subject'),
      '#maxlength' => 180,
    ];
    $form['schedule_cancellation']['mail_scheduled_cancellation_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $config->get('mail_scheduled_cancellation_body'),
      '#rows' => 10,
    ];

    $form['last_message'] = [
      '#markup' => $this->t('Remember always flush the cache after save this
        configuration form.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate return URL pages path.
    // dump(($form_state->getValue('success_url')))
    if (!$this->pathValidator->isValid($form_state->getValue('success_url'))) {
      $form_state->setErrorByName('success_url', $this->t('Either the path 
      %path is invalid or you do not have access to it.', [
        '%path' => $form_state->getValue('success_url')
      ]));
    }
    if (!$this->pathValidator->isValid($form_state->getValue('error_url'))) {
      $form_state->setErrorByName('error_url', $this->t('Either the path %path 
      is invalid or you do not have access to it.', [
        '%path' => $form_state->getValue('error_url')
      ]));
    }
    parent::validateForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config_keys = [
      'content_types', 'allowed_gateways', 'success_url', 'error_url',
      'mail_canceled_subject', 'mail_canceled_body',
      'mail_scheduled_cancellation_subject', 'mail_scheduled_cancellation_body',
    ];
    $ppss_config = $this->config('ppss.settings');
    foreach ($config_keys as $config_key) {
      if ($form_state->hasValue($config_key)) {

        if ($config_key == 'content_types') {
          $ppss_config->set($config_key, array_filter($form_state->getValue(
            $config_key
          )));
        }
        else {
          $ppss_config->set($config_key, $form_state->getValue($config_key));
        }
      }
      $ppss_config->save();
    }
    parent::submitForm($form, $form_state);
  }

  /**
   * Returns a list of all the content types currently installed.
   *
   * @return array
   *   An array of content types.
   */
  public function getExistingContentTypes() {
    $types = [];
    $contentTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($contentTypes as $contentType) {
      $types[$contentType->id()] = $contentType->label();
    }
    return $types;
  }

}
